<?php

class ConversationsController extends BaseController {

	public function __construct(){

		$this->beforeFilter('auth', array());

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$conversations = array();

		$participations = Participant::where('user_id', '=', Auth::user()->getKey())->get();

		foreach($participations as $participation):

			$messages =  Message::where('conversation_id', '=', $participation->conversation_id)->orderBy('created_at', 'asc')->lists('id');

			$conversation = array(
				'id' => $participation->conversation_id,
				'user_id' => $participation->conversation->user_id,
				'messages' => $messages,
				'latest_message' => last($messages)
			);

			foreach($participation->conversation->participants as $participant):
				$conversation['participants'][] = $participant->user_id;
 			endforeach;

			$conversations[] = $conversation;

		endforeach;


		return array('conversations' => $conversations);


	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = array(
			'user_id' => Auth::user()->getKey()
		);

		$conversation = new Conversation($data);

		if($conversation->save()):

			$participants = Input::get('participants');

			if(isset($participants) && is_array($participants) && count($participants) > 1):

				if(in_array($conversation->user_id, $participants)):

					foreach($participants as $participant_id):

						$data = array(
							'user_id' => $participant_id,
							'conversation_id' => $conversation->getKey()
						);

						$participant = new Participant($data);
						$participant->save();

						return array(
							'conversation' => $conversation->toArray()
						);

					endforeach;

				else:

					$response = array(
						'message' => 'The conversation owner has to be a participant.',
						'code' => 500
					);
					return Response::json($response, 500);


				endif;

			elseif(isset($participants) && is_array($participants) && count($participants) < 1):

				$response = array(
					'message' => 'Provide at least 2 participants.',
					'code' => 500
				);
				return Response::json($response, 500);

			else:

				$response = array(
					'message' => 'No participants specified.',
					'code' => 500
				);
				return Response::json($response, 500);


			endif;

		else:

			return $conversation->errors;

		endif;

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$conversation = Conversation::find($id);

		if(!$conversation):

			$response = array(
				'message' => 'Not Found',
				'code'=> 404
			);

			return Response::json($response, 404);

		elseif(!$conversation->isParticipating()):

			$response = array(
				'message' => 'Forbidden',
				'code' => 403
			);

			return Response::json($response, 403);

		endif;

		$messages = $conversation->messages->toArray();
		$conversation = $conversation->toArray();

		$conversation['messages'] = array();

		foreach ($messages as $message) {
			array_push($conversation['messages'], $message['id']);
		}

		return array(
			'conversation' => $conversation
		);



	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$conversation = Conversation::find($id);
		$conversation->delete();

		$participants = Participant::where('conversation_id', $id);
		$participants->delete();

	}






}
