<?php

class UsersController extends BaseController {

	public function __construct(){

		$this->beforeFilter('auth', array('except' => array('login', 'store')));

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$ids = Input::get('ids');
		$users = ($ids) ? User::whereIn('id', $ids)->get() : User::all();

		foreach($users as $user):

			$ids = array();

			foreach($user->participants as $participant):

				$id = $participant->conversation_id;

				if(!in_array($id, $ids))
					$ids[] = $id;

			endforeach;

			unset($user->participants);
			$user->conversations = $ids;
		endforeach;

        return array('users' => $users->toArray());
	}

	/**
	 * User login
	 *
	 * @return Response
	 */
	public function login()
	{

		 $credentials = array(
	        'email' => Input::get('email'),
	        'password' => Input::get('password')
    	);

		if(Auth::attempt($credentials)):

			return Auth::user();

		else:

			$response = array('message' => 'Forbidden');
			return Response::json($response, 403);

		endif;


        return User::all();
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$data = array(
			'email' => Input::get('email'),
			'username' => Input::get('username'),
			'avatar' => Input::get('avatar'),
			'password' => Hash::make(Input::get('password'))
		);

		$user = new User($data);

		if($user->save()):

			return array('user' => $user);

		else:

			return $user->errors;

		endif;


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return array(
        	'user' => User::find($id)->toArray()
        );
	}

	/**
	 * Return Authenticated user
	 *
	 * @return Response
	 */
	public function user()
	{

		if(Auth::check()):

			return array(
				'user' => Auth::user()->toArray()
			);

		else:

			$response = array('message' => 'Forbidden');
			return Response::json($response, 403);

		endif;

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		if(Auth::user()->getKey() != $id):

			$response = array('message' => 'Forbidden');
			return Response::json($response, 403);

		endif;

		$user = User::find($id);

		if(Input::get('email'))
			$user->email = Input::get('email');

		if(Input::get('username'))
			$user->username = Input::get('username');

		if(Input::get('avatar'))
			$user->avatar = Input::get('avatar');

		if(Input::get('password'))
			$user->password = Hash::make(Input::get('password'));

		if($user->save()):

			return $user;

		else:

			return $user->errors;

		endif;

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	public function logout() {

		Auth::logout();

	}

}
