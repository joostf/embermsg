<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends BaseModel implements UserInterface, RemindableInterface {

	protected $table = 'users';
	protected $hidden = array('password');
	protected $fillable = array('email', 'username', 'avatar', 'password', 'created_at', 'updated_at');

	public static $rules = array(
		'username' => 'required|unique:users,username',
		'password' => 'required',
		'email' => 'required|email|unique:users,email'
	);

	public function id()
	{
		return $this->id;
	}

	public function messages(){
        return $this->hasMany('Message');
    }

    public function conversations(){

    	return $this->hasMany('Conversation');

    }

    public function participants(){

    	return $this->hasMany('Participant');
    }

	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	public function getAuthPassword()
	{
		return $this->password;
	}

	public function getReminderEmail()
	{
		return $this->email;
	}


}
