<?php

class Message extends BaseModel {
	protected $guarded = array();
	protected $fillable = array('conversation_id', 'user_id', 'body', 'created_at', 'updated_at');

	public static $rules = array(
		'conversation_id' => 'required',
		'user_id' => 'required',
		'body' => 'required|min:3'
	);

	public function decrypt()
	{
		$this->body = Crypt::decrypt($this->body);
	}


	public function user(){
		return $this->belongsTo('User');
	}

	public function conversation(){
		return $this->belongsTo('Conversation');
	}

}
