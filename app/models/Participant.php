<?php

class Participant extends BaseModel {
	
	protected $guarded = array();
	protected $fillable = array('user_id', 'conversation_id', 'created_at', 'updated_at');

	public static $rules = array(
		'user_id' => 'required',
		'conversation_id' => 'required'
	);

	public function conversation(){
		return $this->belongsTo('Conversation');
	}

	public function user(){
		return $this->belongsTo('User');
	}

}
