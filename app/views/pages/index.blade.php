<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ember message</title>

	 <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>

	<script src="/js/libs/jquery-2.0.3.min.js"></script>
	<script src="/js/libs/jquery.timeago.js"></script>
	<script src="/js/libs/handlebars-1.0.0.js"></script>
	<script src="/js/libs/ember-1.1.2.js"></script>
	<script src="/js/libs/ember-data.js"></script>
	<script src="/js/libs/iscroll-probe-min.js"></script>

	<script src="/js/app.js"></script>
	<script src="/js/store.js"></script>

	<script src="/js/models/user.js"></script>
	<script src="/js/models/conversation.js"></script>
	<script src="/js/models/message.js"></script>

	<script src="/js/views/userView.js"></script>
	<script src="/js/views/usersView.js"></script>
	<script src="/js/views/messageView.js"></script>
	<script src="/js/views/messagesView.js"></script>
	<script src="/js/views/conversationView.js"></script>
	<script src="/js/views/conversationsView.js"></script>
	<script src="/js/views/conversationsListitemView.js"></script>
	<script src="/js/views/conversationsListView.js"></script>
	<script src="/js/views/applicationView.js"></script>
	<script src="/js/views/conversationsNewView.js"></script>

	<script src="/js/routes/applicationRoute.js"></script>
	<script src="/js/routes/indexRoute.js"></script>
	<script src="/js/routes/usersRoute.js"></script>
	<script src="/js/routes/userRoute.js"></script>
	<script src="/js/routes/conversationsRoute.js"></script>
	<script src="/js/routes/conversationsNewRoute.js"></script>
	<script src="/js/routes/conversationRoute.js"></script>
	<script src="/js/routes/loginRoute.js"></script>


	<script src="/js/controllers/applicationController.js"></script>
	<script src="/js/controllers/conversationsNewController.js"></script>
	<script src="/js/controllers/userController.js"></script>
	<script src="/js/controllers/usersController.js"></script>
	<script src="/js/controllers/loginController.js"></script>
	<script src="/js/controllers/conversationsController.js"></script>
	<script src="/js/controllers/conversationController.js"></script>


	<script src="/js/router.js"></script>

	@include('handlebars-templates/loading')
	@include('handlebars-templates/application')
	@include('handlebars-templates/index')
	@include('handlebars-templates/user')
	@include('handlebars-templates/users')
	@include('handlebars-templates/conversation')
	@include('handlebars-templates/conversations')
	@include('handlebars-templates/login')
	@include('handlebars-templates/message')
	@include('handlebars-templates/ConversationsList')
	@include('handlebars-templates/conversationsListitem')
	@include('handlebars-templates/emptyConversationsListitem')
	@include('handlebars-templates/conversationsNew')


</body>
</html>