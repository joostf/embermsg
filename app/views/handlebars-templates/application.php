<script type="text/x-handlebars" id="application">


{{#if App.CurrentUser}}
	{{outlet sidebar}}
	{{outlet main}}
	{{outlet mobile}}
{{else}}

	<div class="loginform">
		{{outlet login}}
	</div>

{{/if}}






</script>