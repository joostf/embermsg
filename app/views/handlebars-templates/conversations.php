<script type="text/x-handlebars" id="conversations">

	<div class='header'>

		{{#if showEditButton}}
			<button {{action 'hideDelete'}} class="leftcontrol">Gereed</button>
			{{else}}
			<button {{action 'showDelete'}} class="leftcontrol">Wijzig</button>
		{{/if}}

		<h1>{{#link-to 'user' App.CurrentUser.id}}{{title}}{{/link-to}}</h1>

		<button {{action transitionToRoute 'conversations.new'}} class="rightcontrol">Nieuw</button>
	</div>



	<p class="loader" {{bind-attr class="loading:reload"}}>
		<span class="loadermessage">{{loadStatus}} </span>
		<span class='updatedat'>{{updatedAt}}</span>
	</p>

	<div class="conversationsscroller">
		{{view App.ConversationsListView content=model showDeleteControls=showDeleteControls}}
	</div>


</script>