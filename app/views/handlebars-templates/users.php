<script type="text/x-handlebars" id="users">
<div class='header'>
	<button {{action 'backToConversations'}} class="leftcontrol">Gesprekken</button>
	<h1>Gebruikers</h1>
</div>

<ul class="userslist">
	{{#each user in model}}
		<li>
			{{#link-to 'user' user.id}}
				<strong class="username">{{user.username}}</strong>
				<span class='email'>{{user.email}}</span>

			{{/link-to}}
		</li>
	{{/each}}
</ul>

</script>