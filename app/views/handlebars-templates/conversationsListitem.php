<script type="text/x-handlebars" data-template-name="conversationsListitem">


	<span class="confirmdeletebtn" {{action 'confirmDelete' view.content}}></span>

	{{#link-to 'conversation' view.content.id}}



		{{#each user in view.content.participants}}
			{{#unless user.isMe}}
				<strong class="username">{{user.username}}</strong>
			{{/unless}}

		{{/each}}

		{{#with view.content.latest_message}}
			<p class="body">{{body}}</p>
			<span class="date">{{timeago}}</span>
		{{/with}}
	{{/link-to}}

	<span class="deletebtn" {{action 'delete' view.content}}>Verwijder</span>


</script>