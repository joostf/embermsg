App.ConversationController = Ember.ObjectController.extend({

	actions: {
		createMessage: function(){

			var self = this;

			var body = this.get('message');
			var conversationId = this.get('id');

			var message =  this.store.createRecord('message', {
			  body: body,
			  conversation_id: conversationId
			});

			var message = message.save();

			message.then(function(message){
				console.log("saved");
				self.set('message', '');
				self.store.find('conversation');
			})

		},

		backToConversations: function(){
			this.transitionToRoute('conversations');
		},

		reloadMessages: function(){
			var conversation = this.store.all('conversation');
			conversation.update();
		}
	}

});