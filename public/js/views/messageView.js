App.MessageView = Ember.View.extend({
	templateName: 'message',
	tagName: 'li',
	classNames: ['message'],
	classNameBindings: ['isMe:currentuser'],

    init: function(){
        this._super();
    },

    setIsMe: function(){
        var self = this;
        var currentUserId = Ember.get('App.CurrentUser.id');
        var user = this.get('content.user_id');

        user.then(function(user){
            if(user.get('id') == currentUserId){
              self.set('isMe', true);
            }
        });

  }.observes('content.user_id.id').on('init'),

	timeago: function(){

      jQuery.timeago.settings.strings = {
        prefixAgo: null,
        prefixFromNow: "",
        suffixAgo: "geleden",
        suffixFromNow: "van nu",
        seconds: "minder dan een minuut",
        minute: "ongeveer een minuut",
        minutes: "%d minuten",
        hour: "ongeveer een uur",
        hours: "ongeveer %d uur",
        day: "een dag",
        days: "%d dagen",
        month: "ongeveer een maand",
        months: "%d maanden",
        year: "ongeveer een jaar",
        years: "%d jaar",
        wordSeparator: " ",
        numbers: []
      };


      var timestamp = this.get('content').get('created_at');

      var timeago = $.timeago( timestamp );

      return timeago;

    }.property('content.created_at')
});