/* /models/user.js */
App.Conversation = DS.Model.extend({
	messages: DS.hasMany('message', { async: true}),
	latest_message: DS.belongsTo('message', { async: true}),
	participants: DS.hasMany('user', { async: true}),
});
