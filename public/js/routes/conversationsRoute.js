/* /routes/conversationsRoute.js */

App.ConversationsRoute = Ember.Route.extend({
	model: function() {
		return this.store.find('conversation');
	},

	renderTemplate: function() {
		this.render({ outlet: 'sidebar' });
	}
});